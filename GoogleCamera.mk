PRODUCT_PACKAGES += \
    GoogleCamera

PRODUCT_COPY_FILES += \
    vendor/GoogleCamera/privapp-permissions-googlecamera.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-googlecamera.xml
